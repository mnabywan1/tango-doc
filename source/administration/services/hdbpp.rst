:audience:`all`

.. _hdbpp_service:

HDB++ - an archiving/historian service
======================================

Below, details on deployment and configuration of the `HDB++` service are provided.
For overview, see :ref:`hdbpp_manual` of :ref:`tools_index` section.

.. toctree::
   :name: hdbppdesignguidelines
   :maxdepth: 4

   hdbpp/hdb++-design-guidelines
   hdbpp/hdbpp-es-interface
   hdbpp/hdbpp-cm-interface
   hdbpp/hdb-legacy
   hdbpp/hdbpp-timescaledb
   hdbpp/hdbpp-mysql
   hdbpp/hdbpp-cassandra

